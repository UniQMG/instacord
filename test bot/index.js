const Instacord = require('../instacord');
const Logger = require('../instacord/util/logger');
const logger = new Logger("test bot");

var discord = new Instacord();
discord
  .discordjs()
  .mongodb('mongodb://localhost:27017/instacord')
  .login(require('./secret.js').token)
  .cmd("count", discord.load(), (sub, message, actions) => {
    var user = message.storage.user;
    user.count = (user.count || 0) + 1;
    message.reply("Count to " + user.count);
  });
