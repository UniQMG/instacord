const Logger = require('./util/logger');
const logger = new Logger("command");
const util = require('util');

class Command {
  constructor(cmd) {
    this._chain = [];
    this._cmd = cmd;
  }

  chain(...thing) {
    thing.forEach((thing, i) => {
      if (typeof thing != 'function')
        throw new Error(`Expected function as argument ${i}.`);
      this._chain.push(thing);
    })
  }

  test(command, args) {
    var cmd = this._cmd;
    if (typeof(cmd) == 'string' || typeof(cmd) == 'number') {
      //logger.debug(`Interpreting '${command}' as string`);
      if (command.startsWith(cmd))
        return command.substring(cmd.length);
    }
    if (cmd instanceof RegExp) {
      cmd.lastIndex = 0;
      var exec = cmd.exec(command);
      if (exec) return command.substring(exec[0].length);
    }
    if (typeof(cmd) == 'function') {
      var res = cmd(command, ...args);
      if (typeof(res) == 'string') return command.substring(res.length);
      if (typeof(res) == 'number') return command.substring(res);
      if (res === true) return "";
    }
    if (typeof(cmd) == 'undefined' || cmd == null) return '';
    return false;
  }

  resolve(command, ...args) {
    //logger.debug(`Resolve command '${this._cmd}' for '${command}'`);
    command = this.test(command, args);
    if (command === false) return false;

    // Turn callbacks into a promise chain
    var promises = this._chain.map(chainlink => {
      var start;
      var promise = new Promise((resolve, reject) => {
        start = (() => {
          //logger.debug(`Execute middleware for '${command}': '${chainlink.toString()}'`)
          var val = chainlink.call(this, command, ...args);
          //logger.debug(`Command '${command}' returned ${util.inspect(val)}`);
          while (typeof(val) == 'function') val = val();
          if (val instanceof Promise) {
            val.then(function() {
              //logger.debug(`Command '${command}' promise resolved`);// and returned '${util.inspect(...arguments)}'`);
              return resolve(...arguments);
            }).catch(err => {
              //logger.debug(`Command '${command}' promise rejected with error: '${err}'`);
              reject(err);
            });
            return promise;
          }
          if (val === false) {
            //logger.debug(`Command '${command}' returned false`);
            reject();
          }
          resolve(val);
          return promise;
        });
      });
      promise.start = start;
      return promise;
    });

    return new Promise((resolve, reject) => {
      // Link together promise chain
      var lastpromise;
      promises.forEach(iterpromise => {
        if (!lastpromise) {
          lastpromise = iterpromise
          iterpromise.start();
        } else {
          lastpromise.then(() => iterpromise.start());
          lastpromise = iterpromise;
        }
      });
      lastpromise.catch(reject);
      // Resolve the outer promise when the chain is done
      lastpromise.then(() => resolve());
    });
  }
}
module.exports = Command;
