module.exports = (function Abbreviate(thing, length, options) {
  thing = thing.toLowerCase();
  length = Math.max(length || 8, 1);
  options = options || {};

  /* initials strategy */
  var words = thing.replace(/( )?[^ ]+/g, "-").length;
  if (options.initials == true || words >= (options.wordcount || length)) {
    if (options.initials != false) {
        thing = thing.replace(/(?:,? ?)(\S)[^ ]*/g, '$1');
    }
  }

  if (options.reduction != false) {
      /* reduction strategy */
      var filters = [
        /[^A-z0-9_]([A-z0-9_]*)$/, /* Special characters  */
        /(.)\1/,                   /* duplicate chars     */
        /[aeiou]([^aeiou]*)$/,     /* Vowels (not y)      */
        /[^y](.)$/,                /* Everything except y */
        /y([^y]*)$/                /* y                   */
      ];

      var filterid = 0;
      while (thing.length > length) {
        var filter = filters[filterid];
        if (filter.test(thing)) {
          thing = thing.replace(filter, '$1');
          filterid = 0;
          continue;
        }
        filterid = filterid + 1;
      }
  }

  if (options.truncation != false && thing.length > length) {
      thing = thing.substring(0, length);
  }

  if (options.padding != false) {
      var paddingRequired = length - thing.length;
      if (typeof options.padding == 'number')
        paddingRequired = options.padding - filter.length;

      thing = ""
          + " ".repeat(Math.floor(paddingRequired/2))
          + thing
          + " ".repeat(Math.ceil(paddingRequired/2));
  }

  return thing;
});
