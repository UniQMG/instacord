const Mongo = require('../../util/tryrequire')('mongodb');
const logger = new (require('../../util/logger'))("MongoDB backend");
const GenericStore = require('../generic');


class MongoDB extends GenericStore {
  /**
   * Constructs a new MongoDB backend
   * @param url
   * @param dbName
   * @emits ready
   * @emits connectionSuccess
   * @emits connectionFailed
   */
  constructor(url, dbName) {
    super();
    if (!url || !dbName)
      throw new Error("Expected database url and name as arguments");
    this.url = url;
    this.dbName = dbName;

    if (!Mongo) throw new Error("Please install the MongoDB client to use this backend (npm install mongodb --save).")
    const MongoClient = Mongo.MongoClient;
    this.ready = new Promise((resolve, reject) => {
      MongoClient.connect(url, (err, client) => {
        if (err) {
          this.emit("connectionFailed", err);
          logger.critical(err);
          reject(err);
          return;
        }

        this.client = client;
        this.db = client.db(dbName);

        this.client = client;
        this.users = this.db.collection('users');
        this.guilds = this.db.collection('guilds');
        this.channels = this.db.collection('channels');
        this.emit("connectionSuccess", client);
        resolve();
      });
    });
    this.ready.then(() => this.emit('ready'));
  }

  get(id, collection) {
    return this.ready.then(() => {
      return collection.findOne({ _id: id })
    }).then(doc => {
      doc = doc || {};
      doc._id = id;
      doc.save = (() => {
        doc._id = id;
        return collection.save(doc);
      });
      return doc;
    });
  }

  /**
   * Returns the underlying DB wrapper, or null.
   * @return {MongoClient}
   */
  getDatabase() {
    return this.client;
  }

  getUser(id) {
    return this.ready.then(() => this.get(id, this.users));
  }

  getGuild(id) {
    return this.ready.then(() => this.get(id, this.guilds));
  }

  getChannel(id) {
    return this.ready.then(() => this.get(id, this.channels));
  }
}

module.exports = MongoDB;
