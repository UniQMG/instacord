const EventEmitter = require('events').EventEmitter;
const util = require('util');

/**
 * A generic database store adapter
 * @implements {EventEmitter}
 */
class GenericStore {

    /**
     * Constructs a new generic store backend.
     */
    constructor() {
    }

    /**
     * Returns the underlying DB wrapper, or null.
     * @return {Object}
     */
    getDatabase() {
      return null;
    }

    /**
     * Gets a user by ID
     * @return {Object}
     */
    getUser(id) {
      return {};
    }

    /**
     * Gets a guild by ID
     * @return {Object}
     */
    getGuild(id) {
      return {};
    }

    /**
     * Gets a channel by ID
     * @return {Object}
     */
    getChannel(id) {
      return {};
    }
}

util.inherits(GenericStore, EventEmitter);
module.exports = GenericStore;
